//
//  DPGAppDelegate.h
//  DynamicsPlayground
//
//  Created by Paulo Mendes on 2/25/14.
//  Copyright (c) 2014 Movile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DPGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
