//
//  DPGExampleViewController.m
//  DynamicsPlayground
//
//  Created by Paulo Mendes on 3/11/14.
//  Copyright (c) 2014 Movile. All rights reserved.
//

#import "DPGExampleViewController.h"

@interface DPGExampleViewController () <UIGestureRecognizerDelegate, UIDynamicAnimatorDelegate>

@property (nonatomic, strong) UIDynamicAnimator *animator;

@property (weak, nonatomic) IBOutlet UIView *menuView;
@property (weak, nonatomic) IBOutlet UIImageView *openMenuArrow;
@property (weak, nonatomic) IBOutlet UIImageView *closeMenuArrow;
@property (nonatomic, assign) BOOL menuIsOpen;

@end

@implementation DPGExampleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.openMenuArrow.hidden = 1;
    self.menuIsOpen = NO;
    NSLog(@"View Did Load");
}

- (IBAction)closeCoolMenuTap:(id)sender {

    if (self.menuIsOpen) {
        [self closeAnimationOpen];
    } else {
        [self startAnimationOpen];
    }
}

- (void)closeAnimationOpen {
    self.menuIsOpen = NO;
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    self.animator.delegate = self;
    
    UIGravityBehavior *gravity = [[UIGravityBehavior alloc] initWithItems:@[ self.menuView ]];
    gravity.angle = M_PI;
    gravity.magnitude = 2;
    [self.animator addBehavior:gravity];
    
    UIDynamicItemBehavior *itemBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[ self.menuView ]];
    itemBehavior.elasticity = 0.4;
    itemBehavior.density = 1;
    [self.animator addBehavior:itemBehavior];
    
//    UISnapBehavior *snap = [[UISnapBehavior alloc] initWithItem:self.menuView snapToPoint:CGPointMake(-250, 52)];
//    snap.damping = 0.5f;
//    [self.animator addBehavior:snap];
    
    UICollisionBehavior *collision = [[UICollisionBehavior alloc] initWithItems:@[ self.menuView ]];
    CGPoint rightEdgeTop = CGPointMake(-280, 0);
    CGPoint rightEdgeBottom = CGPointMake(-280, 568);
    [collision addBoundaryWithIdentifier:@"barrier" fromPoint:rightEdgeTop toPoint:rightEdgeBottom];
    [self.animator addBehavior:collision];
    
    UIDynamicItemBehavior *item = [[UIDynamicItemBehavior alloc] initWithItems:@[ self.menuView ]];
    item.density = 10;
    itemBehavior.allowsRotation = NO;
    [self.animator addBehavior:item];
    
    [UIView animateWithDuration:0.5 animations:^{
        self.closeMenuArrow.transform = CGAffineTransformMakeRotation(M_PI * 2);
    }];
    
}

- (void)startAnimationOpen {
    self.menuIsOpen = YES;
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    self.animator.delegate = self;
    
    
    UIGravityBehavior *gravity = [[UIGravityBehavior alloc] initWithItems:@[ self.menuView ]];
    gravity.angle = M_PI * 2;
    gravity.magnitude = 2;
    [self.animator addBehavior:gravity];
    
    UICollisionBehavior *collision = [[UICollisionBehavior alloc] initWithItems:@[ self.menuView ]];
    CGPoint rightEdgeTop = CGPointMake(320, 0);
    CGPoint rightEdgeBottom = CGPointMake(320, 568);
    [collision addBoundaryWithIdentifier:@"barrier" fromPoint:rightEdgeTop toPoint:rightEdgeBottom];
    [self.animator addBehavior:collision];
    
    UIDynamicItemBehavior *itemBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[ self.menuView ]];
    itemBehavior.elasticity = 0.4;
    itemBehavior.density = 1;
    itemBehavior.allowsRotation = NO;
    [self.animator addBehavior:itemBehavior];
    
    [UIView animateWithDuration:0.5 animations:^{
        self.closeMenuArrow.transform = CGAffineTransformMakeRotation(M_PI);
    }];
    
}

- (void)dynamicAnimatorDidPause:(UIDynamicAnimator *)animator {
    [self.animator removeAllBehaviors];
}

@end
