//
//  DPGSecondViewController.m
//  DynamicsPlayground
//
//  Created by Paulo Mendes on 3/13/14.
//  Copyright (c) 2014 Movile. All rights reserved.
//

#import "DPGSecondViewController.h"

@interface DPGSecondViewController ()

@property (weak, nonatomic) IBOutlet UIView *square;
@property (nonatomic, strong) UIDynamicAnimator *animator;
@property (weak, nonatomic) IBOutlet UIView *retangle;

@end

@implementation DPGSecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    UIGravityBehavior *gravity = [[UIGravityBehavior alloc] initWithItems:@[ self.retangle ]];
    gravity.magnitude = 1;
    
    [self.animator addBehavior:gravity];
    
    UIOffset offset = UIOffsetMake(20, 20);
    
    UIAttachmentBehavior *attach = [[UIAttachmentBehavior alloc] initWithItem:self.retangle offsetFromCenter:offset attachedToAnchor:CGPointMake(self.square.frame.origin.x, 320/2)];
    
    attach.frequency = 0;
    attach.damping = 1;
    
    [self.animator addBehavior:attach];
    
}

@end
