//
//  DPGViewController.m
//  DynamicsPlayground
//
//  Created by Paulo Mendes on 2/25/14.
//  Copyright (c) 2014 Movile. All rights reserved.
//

#import "DPGViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface DPGViewController () <UICollisionBehaviorDelegate>

@property (nonatomic, strong) UIDynamicAnimator *animator;
@property (nonatomic, strong) UIGravityBehavior *gravity;
@property (nonatomic, strong) UICollisionBehavior *collision;
@property (nonatomic, strong) UIAttachmentBehavior *touchAttachmentBehavior;

@property (nonatomic, assign) BOOL firstContact;

@end

@implementation DPGViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    
    UIView* square = [[UIView alloc] initWithFrame:CGRectMake(100, 40, 100, 100)];
    square.backgroundColor = [UIColor grayColor];
    [self.view addSubview:square];
    
    UIView *barrier = [[UIView alloc] initWithFrame:CGRectMake(0, 300, 130, 20)];
    barrier.backgroundColor = [UIColor blueColor];
    [self.view addSubview:barrier];
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[ square ]];
    self.gravity.angle = 0.5 * M_PI;
    self.gravity.magnitude = 1;
    [self.animator addBehavior:self.gravity];
    
    self.collision = [[ UICollisionBehavior alloc] initWithItems:@[ square ]];
    self.collision.collisionDelegate = self;
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    [self.animator addBehavior:self.collision];
    
    CGPoint rightEdge = CGPointMake(barrier.frame.origin.x + barrier.frame.size.width, barrier.frame.origin.y);
    [self.collision addBoundaryWithIdentifier:@"barrier" fromPoint:barrier.frame.origin toPoint:rightEdge];
    
//    __weak typeof(self) weakSelf = self;
    
//    self.collision.action = ^{
//        NSLog(@"%@, %@", NSStringFromCGAffineTransform(square.transform), NSStringFromCGPoint(square.center));
//        //UIView* square = [[UIView alloc] initWithFrame:CGRectMake(100, 50, 100, 100)];
//        UIView *position = [[UIView alloc] initWithFrame:CGRectMake(square.frame.origin.x, square.frame.origin.y, 100, 100)];
//        position.backgroundColor = [UIColor clearColor];
//        position.layer.transform = square.layer.transform;
//        position.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        position.layer.borderWidth = 0.5f;
//        [weakSelf.view addSubview:position];
//    };
    
    UIDynamicItemBehavior *itemBehaviour = [[UIDynamicItemBehavior alloc] initWithItems:@[ square ]];
    itemBehaviour.elasticity = 0.5;     //determines how ‘elastic’ collisions will be, i.e. how bouncy or ‘rubbery’ the item behaves in collisions.
    itemBehaviour.friction = 0.5;       //determines the amount of resistance to movement when sliding along a surface.
    itemBehaviour.density = 1.5;        //when combined with size, this will give the overall mass of an item. The greater the mass, the harder it is to accelerate or decelerate an object.
    itemBehaviour.resistance = 0.5;       //determines the amount of resistance to any linear movement. This is in contrast to friction, which only applies to sliding movements.
    itemBehaviour.angularResistance = 0.2; //determines the amount of resistance to any rotational movement.
    itemBehaviour.allowsRotation = YES;     //this is an interesting one that doesn’t model any real-world physics property. With this property set to NO the object will not rotate at all, regardless of any rotational forces that occur.
    
    [self.animator addBehavior:itemBehaviour];
}

#pragma mark - Collision Behavior Delegate
- (void)collisionBehavior:(UICollisionBehavior *)behavior beganContactForItem:(id<UIDynamicItem>)item withBoundaryIdentifier:(id<NSCopying>)identifier atPoint:(CGPoint)p {
    NSLog(@"Boudary contact occurred - %@", identifier);
    UIView *view = (UIView *)item;
    
    view.backgroundColor = [UIColor redColor];
    
    [UIView animateWithDuration:0.3 animations:^{
        view.backgroundColor = [UIColor grayColor];
    }];
    
    if (!self.firstContact) {
        self.firstContact = YES;
        
        UIView *square = [[UIView alloc] initWithFrame:CGRectMake(80, 0, 100, 100)];
        square.backgroundColor = [UIColor grayColor];
        [self.view addSubview:square];
        
        [self.collision addItem:square];
        [self.gravity addItem:square];
        
        UIAttachmentBehavior *attach = [[UIAttachmentBehavior alloc] initWithItem:view attachedToItem:square];
        [self.animator addBehavior:attach];
    }
}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    self.touchAttachmentBehavior = [UIAttachmentBehavior alloc] initWithItem
//}

@end
