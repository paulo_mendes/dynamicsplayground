//
//  main.m
//  DynamicsPlayground
//
//  Created by Paulo Mendes on 2/25/14.
//  Copyright (c) 2014 Movile. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DPGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DPGAppDelegate class]));
    }
}
